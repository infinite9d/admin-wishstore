data class ReportUser(
    val created_at: String,
    val created_by: Int,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val id: Int,
    val id_pelapor: Int,
    val id_user: Int,
    val is_deleted: Boolean,
    val keterangan: String,
    val user_dilapor: User,
    val user_pelapor: User
)