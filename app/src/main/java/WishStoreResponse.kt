
import com.google.gson.annotations.SerializedName

data class WishStoreResponse(
    @SerializedName("message")
    val message: String
)