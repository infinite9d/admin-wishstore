import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path

interface TransService {

    @GET("trans")
    fun getTransaction() : Call<List<Transaction>>

    @PATCH("trans/{id}")
    fun updateTransaction(@Path("id") id:Int,@Body data: HashMap<String,String>) : Call<WishStoreResponse>
}