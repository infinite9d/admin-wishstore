import retrofit2.Call
import retrofit2.http.*

interface CategoryService {
    @POST("category")
    fun insertCategory(@Body data: HashMap<String,String>): Call<WishStoreResponse>
    @GET("category")
    fun fetchCategory(): Call<List<Category>>
    @DELETE("category/{id}")
    fun deleteCategory(@Path("id") id:Int): Call<WishStoreResponse>
    @PATCH("category/{id}")
    fun updateCategory(@Path("id") id:Int,@Body data: HashMap<String, String>): Call<WishStoreResponse>
}