package com.example.adminwishstore

import ReportPost
import ReportService
import ReportUser
import adapter.ReportUserAdapter
import adapter.ViewReportPostAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class ActivityReport : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)
        var listpost = arrayListOf<ReportPost>()
        var listuser = arrayListOf<ReportUser>()
        imageback_report.setOnClickListener {
            finish()
        }

        //recycler
        recyclerreport_report.layoutManager = LinearLayoutManager(applicationContext)
        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.getAllReport().await()
                listpost.addAll(response)
                lifecycleScope.launch(Dispatchers.Main){
                    recyclerreport_report.adapter = ViewReportPostAdapter(listpost,applicationContext)
                }
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }

        btnreportpost_report.setOnClickListener {
            btnreportuser_report.setTextColor(resources.getColor(R.color.Black))
            btnreportuser_report.setBackgroundColor(resources.getColor(R.color.Gray))
            btnreportpost_report.setTextColor(resources.getColor(R.color.White))
            btnreportpost_report.setBackgroundColor(resources.getColor(R.color.RoyalBlue))
            listuser.clear()
            listpost.clear()
            recyclerreport_report.layoutManager = LinearLayoutManager(applicationContext)
            val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
            lifecycleScope.launch(Dispatchers.IO){
                try{
                    val response = api.getAllReport().await()
                    listpost.addAll(response)
                    lifecycleScope.launch(Dispatchers.Main){
                        recyclerreport_report.adapter = ViewReportPostAdapter(listpost,applicationContext)
                    }
                }catch (e : HttpException){
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("error",e.message.toString())
                    }
                }
            }
        }
        btnreportuser_report.setOnClickListener {
            btnreportuser_report.setTextColor(resources.getColor(R.color.White))
            btnreportuser_report.setBackgroundColor(resources.getColor(R.color.RoyalBlue))
            btnreportpost_report.setTextColor(resources.getColor(R.color.Black))
            btnreportpost_report.setBackgroundColor(resources.getColor(R.color.Gray))
            listpost.clear()
            listuser.clear()
            recyclerreport_report.layoutManager = LinearLayoutManager(applicationContext)
            val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
            lifecycleScope.launch(Dispatchers.IO){
                try{
                    val response = api.getAllUser().await()
                    listuser.addAll(response)
                    lifecycleScope.launch(Dispatchers.Main){
                        recyclerreport_report.adapter = ReportUserAdapter(listuser,this@ActivityReport)
                    }
                }catch (e : HttpException){
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("error",e.message.toString())
                    }
                }
            }
        }
    }
    public fun updateStatusUser(position : Int){
        val data = hashMapOf(
            "status" to 1
        )

        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.updateStatus(position,data).await()
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
    }
}
