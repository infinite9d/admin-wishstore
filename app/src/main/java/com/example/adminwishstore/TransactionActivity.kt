package com.example.adminwishstore

import TransService
import adapter.TransactionAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_transaction.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class TransactionActivity : AppCompatActivity() {

    var api = ApiClient.getRetrofitInstance(this).create(TransService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction)
        fetchData()
    }
    fun acceptTransaction(id:Int)
    {

        val data = hashMapOf<String,String>(
            "status" to "1"
        )
        lifecycleScope.launch(Dispatchers.IO){
            try {
                val res = api.updateTransaction(id,data).await()
                lifecycleScope.launch(Dispatchers.Main){
                    Toast.makeText(this@TransactionActivity,"Berhasil menerima transaksi",Toast.LENGTH_SHORT).show()
                    fetchData()
                }
            }catch (ex:HttpException)
            {
                lifecycleScope.launch(Dispatchers.Main){
                    Toast.makeText(this@TransactionActivity,"Terjadi kesalahan pada sistem silakan coba kembali",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun declineTransaction(id:Int)
    {

        val data = hashMapOf<String,String>(
            "status" to "2"
        )
        lifecycleScope.launch(Dispatchers.IO){
            try {
                val res = api.updateTransaction(id,data).await()
                lifecycleScope.launch(Dispatchers.Main){
                    Toast.makeText(this@TransactionActivity,"Berhasil membatalkan transaksi",Toast.LENGTH_SHORT).show()
                    fetchData()
                }
            }catch (ex:HttpException)
            {
                lifecycleScope.launch(Dispatchers.Main){
                    Toast.makeText(this@TransactionActivity,"Terjadi kesalahan pada sistem silakan coba kembali",Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
    fun fetchData(){
        lifecycleScope.launch(Dispatchers.IO){
            try {
                var data = api.getTransaction().await()
                data= data.filter {
                    item -> item.status==0
                }
                lifecycleScope.launch(Dispatchers.Main){
                    rvTransaction.adapter= TransactionAdapter(ArrayList(data),this@TransactionActivity)
                }
            }catch (ex :HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Toast.makeText(this@TransactionActivity,ex.response()?.body().toString(),Toast.LENGTH_LONG).show()
                    Log.w("coba",ex.response().toString())
                    Log.w("coba",ex.response()?.body().toString())
                }
            }
        }
    }
}
