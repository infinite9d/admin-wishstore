package com.example.adminwishstore

import ReportPost
import ReportService
import adapter.ViewReportPostAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_view_report_posting.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class ActivityViewReportPosting : AppCompatActivity() {

    lateinit var listpost:ArrayList<ReportPost>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_report_posting)
        listpost = ArrayList()
        recyclerreport_reportpost.layoutManager = LinearLayoutManager(applicationContext)
        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.getAllReport().await()
                listpost.addAll(response)
                lifecycleScope.launch(Dispatchers.Main){
                    recyclerreport_reportpost.adapter = ViewReportPostAdapter(listpost,applicationContext)
                }
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
        imageback_reportpost.setOnClickListener {
            finish()
        }
    }
}
