package com.example.adminwishstore

import Category
import CategoryService
import adapter.ListCategoryAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.LinearLayout
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_list_category.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class ActivityListCategory : AppCompatActivity() {
    var listcategory = arrayListOf<Category>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_category)
        recyclerlistcategory.layoutManager = LinearLayoutManager(applicationContext)
        fetchAllCategory()
        btnadd_category.setOnClickListener {
            val intent = Intent(applicationContext,ActivityMasterCategory::class.java)
        }

        btnadd_category.setOnClickListener {
            val intent = Intent(applicationContext,ActivityMasterCategory::class.java)
            startActivity(intent)
        }
    }
    fun fetchAllCategory(){
        listcategory.clear()
        val api = ApiClient.getRetrofitInstance(applicationContext).create(CategoryService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
        try {
            val response = api.fetchCategory().await()
            listcategory.addAll(response)
            lifecycleScope.launch(Dispatchers.Main){
                recyclerlistcategory.adapter = ListCategoryAdapter(listcategory,this@ActivityListCategory)
            }
        }catch (e : HttpException){
            lifecycleScope.launch(Dispatchers.Main){
                Log.d("error",e.message.toString())
            }
        }
    }
    }
    fun deleteCategory(position: Int){
        val api = ApiClient.getRetrofitInstance(applicationContext).create(CategoryService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try {
                val response = api.deleteCategory(listcategory.get(position).id).await()
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("success",response.toString())
                    Toast.makeText(applicationContext,"Delete Success",Toast.LENGTH_SHORT).show()
                    fetchAllCategory()
                }
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
    }
}
