import java.io.Serializable

data class User(
    val alamat: Any?,
    val gender: Boolean,
    val id: Int,
    val id_onesignal: String,
    val kota: Any?,
    val nama: String,
    val username: String
):Serializable