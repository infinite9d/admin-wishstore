package adapter;

import ReportPost
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.adminwishstore.R
import com.example.adminwishstore.activity_detailreport
import kotlinx.android.synthetic.main.row_reportpost.view.*

class ViewReportPostAdapter (val items : ArrayList<ReportPost>, val context: Context): RecyclerView.Adapter<ViewReportPostAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_reportpost,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val report = items.get(position)
        holder.user_posted.text = "By: "+report.post_user.nama
        holder.product_name.text = report.post.judul
        holder.user_reporter.text = "Reporter: "+report.user.nama
        holder.information.text = report.keterangan
        holder.image.load(report.post.url_photo)
        holder.layout.setOnClickListener {
            val intentdetail = Intent(context,activity_detailreport::class.java)
            intentdetail.putExtra("report",report)
            context.startActivity(intentdetail)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val user_posted = view.tvby_reportpost
        val product_name = view.tvproductname_reportpost
        val user_reporter = view.tvreporter_reportpost
        val image = view.imageproduct_reportpost
        val information = view.tvinformation_reportpost
        val layout = view.layout_reportpost
    }

}