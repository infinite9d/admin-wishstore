package com.example.adminwishstore

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    fun OpenTransaction(v :View){
        val intent = Intent(this,TransactionActivity::class.java)
        startActivity(intent)
    }
    fun ViewReportPost(v : View) {
        startActivity(Intent(this, ActivityViewReportPosting::class.java))
    }

    fun Report(v : View) {
        startActivity(Intent(this, ActivityReport::class.java))
    }

    fun HidePost(v : View) {
        startActivity(Intent(this, ActivityHidePost::class.java))
    }

    fun MasterCategory(v : View) {
        startActivity(Intent(this, ActivityListCategory::class.java))
    }
}
