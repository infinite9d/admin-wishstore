package adapter;

import Post
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import kotlinx.android.synthetic.main.row_hidepost.view.*
import com.example.adminwishstore.ActivityHidePost
import com.example.adminwishstore.R


class PostAdapter (val items : ArrayList<Post>, val context: Context): RecyclerView.Adapter<PostAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_hidepost,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val report = items.get(position)
        holder.user_posted.text = "By: "+report.user_created.nama
        holder.product_name.text = report.judul
        holder.information.text = report.deskripsi
        holder.image.load(report.url_photo)
        if(report.status==2)
            holder.switch.isChecked=true
        holder.switch.setOnClickListener {
            (context as ActivityHidePost).hideAPost(holder.switch,report.id)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val user_posted = view.tvby_hidereport
        val product_name = view.tvproductname_hidereport
        val image = view.imageproduct_hidereport
        val information = view.tvinformation_hidereport
        val layout = view.layout_hidereport
        val switch = view.switch_hidereport
    }

}