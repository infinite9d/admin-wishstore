package adapter

import Category
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.adminwishstore.ActivityEditCategory
import com.example.adminwishstore.ActivityListCategory
import com.example.adminwishstore.R
import kotlinx.android.synthetic.main.row_list_category.view.*
import java.util.*
import kotlin.collections.ArrayList

class ListCategoryAdapter(val items : ArrayList<Category>, val context: Context): RecyclerView.Adapter<ListCategoryAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup,viewType: Int): ListCategoryAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_list_category,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ListCategoryAdapter.ViewHolder, position: Int) {
        val category = items.get(position)
        holder.image.load(category.url_photo)
        holder.judul.text = category.nama
        holder.btnedit.setOnClickListener {
            val intent = Intent(context, ActivityEditCategory::class.java)
            intent.putExtra("category",category)
            context.startActivity(intent)
        }
        holder.btndelete.setOnClickListener {
            (context as ActivityListCategory).deleteCategory(position)
        }
    }
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val image = view.category_img
        val judul = view.category_judul
        val btnedit = view.category_edit
        val btndelete = view.category_delete
    }
}