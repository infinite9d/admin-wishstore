package com.example.adminwishstore

import ReportPost
import ReportService
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.lifecycleScope
import coil.api.load
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_detailreport.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class activity_detailreport : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detailreport)
        val report = intent.getSerializableExtra("report") as ReportPost
        imageproduct_detailreport.load(report.post.url_photo)
        tvdetail_detailreport.text =
            "Name: \t\t\t\t\t"+report.post.judul+
            "\nCategory: \t\t\t"+report.post.category.nama+
            "\nDescription: "+report.post.deskripsi+
            "\nStatus: \t\t\t\t\t"+report.post.status+
            "\nOwner: \t\t\t\t\t"+report.post_user.nama
        btnban_detailreport.setOnClickListener {
            //ban item disini
            val data = hashMapOf(
                "status" to 1
            )

            val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
            lifecycleScope.launch(Dispatchers.IO){
                try{
                    val response = api.updatePost(report.post.id,data).await()
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("error",response.toString())
                    }
                }catch (e : HttpException){
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("error",e.message.toString())
                    }
                }
            }
        }
        toolbar_detailreport.setNavigationOnClickListener {
            finish()
        }
    }
}
