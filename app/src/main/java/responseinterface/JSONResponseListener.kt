import org.json.JSONObject

interface JSONResponseListener {
    //fun onError(err: VolleyError)
    fun onResponse(response: JSONObject)
}