import java.io.Serializable

data class Post(
    val category: Category,
    val comment_count: Int,
    val created_at: String,
    val created_by: Int,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val deskripsi: String,
    val id: Int,
    val id_category: Int,
    val is_deleted: Boolean,
    val judul: String,
    val rating_average: Double,
    val status: Int,
    val url_photo: String,
    val url_video: Any?,
    val user_alamat: Any?,
    val user_created: User,
    val user_kota: Any?,
    val wishlist_count: Int
) : Serializable