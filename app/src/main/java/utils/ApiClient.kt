package com.example.wishstore

import android.content.Context
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import java.util.concurrent.TimeUnit


class ApiClient {
    companion object{
        var retrofit: Retrofit? = null
        val BASE_URL = "https://wishstore.azabserver.xyz/"
        fun getRetrofitInstance(context: Context): Retrofit {
            val client = OkHttpClient().newBuilder()
                .addInterceptor(HeaderIntercepter(context))
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()
            if (retrofit == null) {
                retrofit = retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit!!
        }
    }
}
class HeaderIntercepter : Interceptor {
    val context:Context
    constructor(context: Context){
        this.context=context
    }
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val tokenRequest = request.newBuilder()
            .addHeader("token", "token777")
            .build()
        return chain
            .proceed(tokenRequest)
        return chain.proceed(request)
    }

}