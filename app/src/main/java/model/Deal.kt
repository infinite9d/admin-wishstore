data class Deal(
    val accepted_at: Any?,
    val created_at: String,
    val created_by: Int,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val harga: Int,
    val id: Int,
    val id_penawar: Int,
    val id_post: Int,
    val id_poster: Int,
    val is_deleted: Boolean,
    val judul: String,
    val keterangan: String,
    val post: Post,
    val status: Int,
    val url_photo: Any?,
    val user_penawar: UserPenawar,
    val user_poster: UserPoster
)