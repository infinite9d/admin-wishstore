data class UserPenawar(
    val alamat: String,
    val gender: Boolean,
    val id: Int,
    val id_onesignal: String,
    val kota: String,
    val nama: String,
    val no_rekening: String,
    val username: String
)