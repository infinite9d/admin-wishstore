package adapter

import Transaction
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.adminwishstore.R
import com.example.adminwishstore.TransactionActivity
import kotlinx.android.synthetic.main.card_transaction.view.*
import com.example.adminwishstore.FullscreenImageActivity
import android.os.Bundle
import androidx.core.graphics.drawable.toBitmap
import coil.api.load


class TransactionAdapter(val items : ArrayList<Transaction>, val context: Context) : RecyclerView.Adapter<TransactionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.card_transaction, parent, false)
        )
     }

    override fun getItemCount(): Int {
        return if (items ==null)  0 else items.size
    }

    override fun onBindViewHolder(holder: TransactionAdapter.ViewHolder, position: Int) {
        val temp = items.get(position)
        if(temp.url_photo!=null){
            holder.image.load(temp.url_photo)
        }
        holder.judul.setText(temp.deal.post.judul)
        holder.harga.setText("Rp. "+ temp.deal.harga.toString())
        holder.tanggal.setText(temp.created_at.toString())
        holder.btnAccept.setOnClickListener {
            (context as TransactionActivity).acceptTransaction(temp.id)
        }
        holder.btnDecline.setOnClickListener {
            (context as TransactionActivity).declineTransaction(temp.id)
        }
        holder.image.setOnClickListener {
            val intent = Intent(context,FullscreenImageActivity::class.java)
            val image = holder.image.drawable.toBitmap()
            val extras = Bundle()
            extras.putParcelable("imagebitmap", image);
            intent.putExtras(extras)
            context.startActivity(intent)
        }
//        holder.image.setOnClickListener {
//            holder.image.setLayoutParams(
//                LinearLayout.LayoutParams(
//                    LinearLayout.LayoutParams.MATCH_PARENT,
//                    LinearLayout.LayoutParams.MATCH_PARENT
//                )
//            )
//            holder.image.setScaleType(ImageView.ScaleType.FIT_XY)
//        }
    }
    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val image = view.category_img
        val judul = view.category_judul
        val harga = view.transaction_harga
        val tanggal = view.transaction_tanggal
        val btnAccept = view.category_edit
        val btnDecline = view.category_delete
    }
}