package com.example.adminwishstore

import Category
import CategoryService
import adapter.ListCategoryAdapter
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import coil.api.load
import com.example.wishstore.ApiClient
import com.example.wishstore.ImageHelper
import kotlinx.android.synthetic.main.activity_edit_category.*
import kotlinx.android.synthetic.main.activity_list_category.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await
import java.util.*

class ActivityEditCategory : AppCompatActivity() {
    var photos: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_category)
        val category = intent.getSerializableExtra("category") as Category
        tvEditCategory_Name.setText(category.nama)
        imagecategory_editcategory.load(category.url_photo)

        imagecategory_editcategory.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select a file"), 1)
        }
        btnEdit_EditCategory.setOnClickListener{
            val api = ApiClient.getRetrofitInstance(applicationContext).create(CategoryService::class.java)
            lifecycleScope.launch(Dispatchers.IO){
                val data = hashMapOf(
                    "nama" to tvEditCategory_Name.text.toString(),
                    "base64" to photos
                )
                try {
                    val response = api.updateCategory(category.id,data).await()
                    lifecycleScope.launch(Dispatchers.Main){
                        Toast.makeText(applicationContext,response.toString(),Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }catch (e : HttpException){
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("error",e.message.toString())
                    }
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedFile = data?.data
        imagecategory_editcategory.setImageURI(selectedFile)
        photos= ImageHelper.encodeBase64(data!!,contentResolver)
    }
}
