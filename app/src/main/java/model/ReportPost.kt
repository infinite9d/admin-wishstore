import java.io.Serializable

data class ReportPost(
    val created_at: String,
    val created_by: Int,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val id: Int,
    val id_post: Int,
    val id_user: Int,
    val is_deleted: Boolean,
    val keterangan: String,
    val post: Post,
    val post_user: User,
    val user: User
) : Serializable