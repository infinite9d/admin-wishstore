import java.io.Serializable

data class Category(
    val created_at: String,
    val created_by: Int,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val id: Int,
    val is_deleted: Boolean,
    val nama: String,
    val url_photo: String
) : Serializable