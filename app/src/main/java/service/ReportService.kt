import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PATCH
import retrofit2.http.Path

interface ReportService {
    @GET("reportpost")
    fun getAllReport(): Call<List<ReportPost>>
    @GET("reportuser")
    fun getAllUser(): Call<List<ReportUser>>
    @GET("post/page/1/$")
    fun getAllPostPage(): Call<List<Post>>
    @PATCH("user/{id}")
    fun updateStatus(@Path("id") id:Int,@Body data: HashMap<String,Int>) : Call<WishStoreResponse>
    @PATCH("post/{id}")
    fun updatePost(@Path("id") id:Int,@Body data: HashMap<String,Int>) : Call<WishStoreResponse>
}