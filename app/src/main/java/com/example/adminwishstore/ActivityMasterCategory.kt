package com.example.adminwishstore

import CategoryService
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.wishstore.ApiClient
import com.example.wishstore.ImageHelper
import kotlinx.android.synthetic.main.activity_add_category.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class ActivityMasterCategory : AppCompatActivity() {

    var photos:String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_category)
        image_AddCategory.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(Intent.createChooser(intent, "Select a file"), 1)
        }
        btnAdd_AddCategory.setOnClickListener {
            val data = hashMapOf(
                "nama" to tvname_addcategory.text.toString(),
                "base64" to photos
            )
            val api = ApiClient.getRetrofitInstance(applicationContext).create(CategoryService::class.java)
            lifecycleScope.launch(Dispatchers.IO){
                try {
                    val response = api.insertCategory(data).await()
                    lifecycleScope.launch(Dispatchers.Main){
                        Toast.makeText(applicationContext,"Insert Success", Toast.LENGTH_SHORT).show()
                        finish();
                    }
                }catch (e : HttpException){
                    lifecycleScope.launch(Dispatchers.Main){
                        Log.d("base64",photos)
                        Log.d("error",e.message.toString())
                    }
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val selectedFile = data?.data
        image_AddCategory.setImageURI(selectedFile)
        photos= ImageHelper.encodeBase64(data!!,contentResolver)
    }
}
