data class Transaction(
    val alamat: String,
    val created_at: String,
    val created_by: Int,
    val deal: Deal,
    val deleted_at: Any?,
    val deleted_by: Any?,
    val id: Int,
    val id_deal: Int,
    val is_deleted: Boolean,
    val keterangan: String,
    val kota: String,
    val status: Int,
    val url_photo: String?
)