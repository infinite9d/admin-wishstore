package com.example.adminwishstore

import Post
import ReportService
import adapter.PostAdapter
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.activity_hide_post.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.await

class ActivityHidePost : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hide_post)
        imageback_hidepost.setOnClickListener {
            finish()
        }
        var listpost = arrayListOf<Post>()
        recyclerreport_hidepost.layoutManager = LinearLayoutManager(applicationContext)
        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.getAllPostPage().await()
                listpost.addAll(response)
                lifecycleScope.launch(Dispatchers.Main){
                    recyclerreport_hidepost.adapter = PostAdapter(listpost,this@ActivityHidePost)
                }
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
    }
    fun hideAPost(switch: Switch,position: Int){
        val builder = AlertDialog.Builder(this@ActivityHidePost)

        builder.setTitle("Hide Post")
        builder.setMessage("Are you sure want to hide this post?")
        builder.setPositiveButton("YES"){_,_ ->
            if(switch.isChecked)
                updateStatusPost(position)
            else
                unhideStatusPost(position)
        }
        builder.setNegativeButton("No"){_,_ ->
            switch.isChecked =!switch.isChecked
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

    fun updateStatusPost(position : Int){
        val data = hashMapOf(
            "status" to 2
        )
        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.updatePost(position,data).await()
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
    }

    fun unhideStatusPost(position : Int){
        val data = hashMapOf(
            "status" to 0
        )
        val api = ApiClient.getRetrofitInstance(applicationContext).create(ReportService::class.java)
        lifecycleScope.launch(Dispatchers.IO){
            try{
                val response = api.updatePost(position,data).await()
            }catch (e : HttpException){
                lifecycleScope.launch(Dispatchers.Main){
                    Log.d("error",e.message.toString())
                }
            }
        }
    }
}
