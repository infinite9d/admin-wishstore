package adapter;

import ReportPost
import ReportService
import ReportUser
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.example.adminwishstore.ActivityReport
import com.example.adminwishstore.R
import com.example.adminwishstore.activity_detailreport
import com.example.wishstore.ApiClient
import kotlinx.android.synthetic.main.row_reportpost.view.*
import kotlinx.android.synthetic.main.row_reportuser.view.*
import kotlinx.coroutines.Dispatchers
import retrofit2.HttpException
import retrofit2.await

class ReportUserAdapter (val items : ArrayList<ReportUser>, val context: Context): RecyclerView.Adapter<ReportUserAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.row_reportuser,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val report = items.get(position)
        holder.user_posted.text = "By: "+report.user_dilapor.nama
        holder.user_reporter.text = report.user_pelapor.nama
        holder.information.text = report.keterangan
        holder.button.setOnClickListener {
            //update status
            (context as ActivityReport).updateStatusUser(report.id_user)
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view){
        val user_posted = view.tvby_reportuser
        val user_reporter = view.tvname_reportuser
        val image = view.imageuser_reportuser
        val information = view.tvinformation_reportuser
        val layout = view.layout_reportuser
        val button = view.btnban_reportuser
    }

}